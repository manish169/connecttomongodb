const bodyParser = require('body-parser');

const express = require('express');
const app = express();
// const {mongoDB, mongoDBData} = require('./mongodb')
// const mongodb = require('./mongodb')
const mongoDB = require('./mongodb').mongodb
const client = mongoDB.client;
const db = mongoDB.db;

console.log(client)

const port = process.env.port || 3000;
// Get the list of all the Documents
app.get('/', (req, res) => {

    const mongoDB = client.db(dbName);
    const mongoDBData = mongoDB.collection('exampleCOllection').find().toArray();

    mongoDBData
        .then( (value) => {
            console.log(value)
        })
        .catch( (error) => {
            console.log(error)
        })
})

app.post('/', (req, res) => {
    MongoClient.connect(url, { useNewUrlParser:true },  async (error, client) => {
        // Create a collection we want to drop later
        const col = client.db(dbName).collection('exampleCOllection');
    
    
        // Find all duplicate records and delete them.
        // ==========================================
        
        //'allProductsArray' returns a '[object Promise]'
        // var allProductsArray = client.db(dbName).collection('exampleCOllection').find( { items: 'canvas' }).toArray();
    
        // // Converting '[object Promise]' to Display on Screen.
        // allProductsArray
        //     .then( (value) => {
        //         // Remove all the documents so found in the Database
        //         client.db(dbName).collection('exampleCOllection').deleteMany(allProductsArray)
        //         console.log(value)
        //     })
        //     .catch( (error) => {
        //         console.log(error)
        //     })
    
        for (let i =0; i <= 6; i++) {
            await client.db(dbName).collection('exampleCOllection').insertOne({
                items: req.params.name,
                qty: req.params.qty,
                tags: req.params.tags,
                size: req.params.size
            })
        }
    })
        
})

// List all the available databases using admin databases.

/*

const MongoClient = require('mongodb').MongoClient;
const test = require('assert');
// const client = 

// Connection URL
const url = 'mongodb://127.0.0.1:27017/users';
// Database Name
const dbName = 'users';

// COnnect using MongoClient
MongoClient.connect(url, { useNewUrlParser:true }, (error, client) => {
    if (error) {
        console.log('Error in Connecting database...')
    }
    const adminDb = client.db(dbName).admin();

    console.log('Connected to Databases sucessfully...')

    // List all the available databases
    adminDb.listDatabases(function(err, dbs) {
        // test.equal(null, err);
        test.ok(dbs.databases.length > 0);
        console.log( 'Databases:' + JSON.stringify(dbs.databases) )
        client.close();
    })

    
})
*/


const MongoClient = require('mongodb').MongoClient;
const test = require('assert');
const mongodb = require('./mongodb.js')

// Connection URL
const url = 'mongodb://127.0.0.1:27017/users';
// Database Name
const dbName = 'users';

app.use(bodyParser.json())

// COnnect using MongoClient
MongoClient.connect(url, { useNewUrlParser:true },  async (error, client) => {
    // Create a collection we want to drop later
    const col = client.db(dbName).collection('exampleCOllection');


    // Find all duplicate records and delete them.
    // ==========================================
    
    //'allProductsArray' returns a '[object Promise]'
    // var allProductsArray = client.db(dbName).collection('exampleCOllection').find( { items: 'canvas' }).toArray();

    // // Converting '[object Promise]' to Display on Screen.
    // allProductsArray
    //     .then( (value) => {
    //         // Remove all the documents so found in the Database
    //         client.db(dbName).collection('exampleCOllection').deleteMany(allProductsArray)
    //         console.log(value)
    //     })
    //     .catch( (error) => {
    //         console.log(error)
    //     })
    newItem = [
        {
            item: 'chair',
            qty: 75,
            tags: 'D',
            size: { h: 22.85, w: 30, uom:'cm'}
        },
        {
            item: 'table',
            qty: 71,
            tags: 'C',
            size: { h: 22, w: 40, uom:'cm'}
        },
        {
            item: 'drawer',
            qty: 50,
            tags: 'B',
            size: { h: 85, w: 70, uom:'mm'}
        },
        {
            item: 'Monitor',
            qty: 10,
            tags: 'A',
            size: { h: 20, w: 90, uom:'hm'}
        },
    ]

    // for (let i =0; i <= 3; i++) {
    //     await client.db(dbName).collection('exampleCOllection').insertOne({
    //         items: newItem[i].item,
    //         qty: newItem[i].qty,
    //         tags: newItem[i].tags,
    //         size: newItem[i].size
    //     })
    // }
    // Inserting a single document
    // ===========================


    // // Can two await work together
    // await client.db(dbName).collection('exampleCOllection').insertOne({
    //     items: 'Chair',
    //     qty: 200,
    //     tags: ['Fibre'],
    //     size: { h: 12, w: 34.5, uom: 'mm'}
    // })

    // //  Can three await work together
    // await client.db(dbName).collection('exampleCOllection').insertOne({
    //     items: 'Table',
    //     qty: 400,
    //     tags: ['Wood'],
    //     size: { h: 23, w: 44.5, uom: 'cm'}
    // })

    // Querying Documents
    // ==================
    // await client.db(dbName).collection('exampleCOllection').insertMany([
    //     {
    //         item: 'journal',
    //         qty: 25,
    //         size: { h: 14, w: 21, uom: 'cm'},
    //         status: 'A'
    //     },
    //     {
    //         item: 'notebook',
    //         qty: 50,
    //         size: { h: 8.5, w: 11, uom: 'in'},
    //         status: 'B'
    //     },
    //     {
    //         item: 'planner',
    //         qty: 75,
    //         size: { h: 8.5, w: 11, uom: 'cm'},
    //         status: 'D'
    //     },
    //     {
    //         item: 'postcard',
    //         qty: 45,
    //         size: { h: 8.5, w: 11, uom: 'cm'},
    //         status: 'A'
    //     },

    // ])


})

// // Assert Error
// var assert = require('assert')
// console.log( assert.equal( 1 , 2 ) )

app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})